#!/usr/bin/env python3
from typing import *

from io import BufferedRandom, BytesIO, StringIO
import sys
import time

import py_cui # type: ignore

from porth import KEYWORD_NAMES, SIM_NULL_POINTER_PADDING, MEM_VIEW_LIMIT, Program, TokenType
from porth import parse_program_from_file
from porth import simulate_little_endian_linux

__version__ = "0.1.0"

class DebuggerStdin( BufferedRandom ):
    def __init__(self, root: py_cui.PyCUI):
        super().__init__(raw=BytesIO())
        #self.root = root
        self._input = ""

    def readline(self):
        self.root.set_selected_widget(self.root.stdin)
        input = self.root.stdin.get()

        return input

    def _handle_popup(self, input: str):
        self._input = input


class PorthDebugger:

    def __init__(self, root: py_cui.PyCUI, program_path: str, include_paths: List[str], expansion_limit: int, argv: List[str], memory_start: int, memory_range: int):
        """
        Setup Layout, Hotkeys and get parsed Program
        """
        # Init management variable
        self._selected_index = 0
        self._current_break = 0;
        self._memory_start = memory_start
        self._memory_range = memory_range

        self.argv = argv

        # Wrapper class takes its parent PyCUI object
        self.root = root

        # Set Layout and add to element list
        self.elements = []

        self.source_menu = self.root.add_scroll_menu('Source Code', 0, 0, row_span=5, column_span=5)
        self.root.set_status_bar_text('Quit - q | Run - r | Next Step - n | Prev Step - b | Navigation - Arrow Keys | Deselect Widget - ESC | Select Widget - ENTER')
        self.elements.append(self.source_menu)

        self.stack_menu = self.root.add_scroll_menu("Stack", 0, 5, row_span=5, column_span=1)
        self.elements.append(self.stack_menu)

        self.memory_menu = self.root.add_scroll_menu('Memory', 0, 6, row_span=5, column_span=4)
        self.elements.append(self.memory_menu)

        self.stdout = self.root.add_scroll_menu('Console Output', 5, 0, row_span=4, column_span=10)
        self.elements.append(self.stdout)

        self.stdin = self.root.add_text_box('Console Input', 9, 0, row_span=1, column_span=10)
        self.elements.append(self.stdin)

        # Set autofocus to source code
        #self.root.move_focus(self.source_menu)

        self.source_menu.add_key_command(py_cui.keys.KEY_ENTER, self.select_break)

        # Set hotkeys for everyone
        self.root.add_key_command(py_cui.keys.KEY_R_LOWER, self.run_until_break)
        self.root.add_key_command(py_cui.keys.KEY_N_LOWER, self.run_next)
        self.root.add_key_command(py_cui.keys.KEY_B_LOWER, self.run_prev)

        for ele in self.elements:
            ele.add_key_command(py_cui.keys.KEY_R_LOWER, self.run_until_break)
            ele.add_key_command(py_cui.keys.KEY_N_LOWER, self.run_next)
            ele.add_key_command(py_cui.keys.KEY_B_LOWER, self.run_prev)

        # Add Syntax Highlighting
        for keyword in KEYWORD_NAMES.values():
            self.source_menu.add_text_color_rule(keyword, py_cui.YELLOW_ON_BLACK, 'startswith', match_type="line")

        self.source_menu.add_text_color_rule("<Break> ", py_cui.RED_ON_BLACK, 'startswith', match_type="line")
        self.source_menu.add_text_color_rule("", py_cui.CYAN_ON_BLACK, 'startswith', match_type="line")


        # Init stack and memory
        self.stack = List[int]
        self.mem = bytearray(1 + 640_000 + 640_000 + 640_000)


        # Parse Program
        self.program = parse_program_from_file(program_path, include_paths, expansion_limit);

        self.ops = []
        for op in self.program.ops:
            token = op.token

            if token.typ == TokenType.WORD:
                self.ops.append(token.text)
            elif token.typ == TokenType.INT:
                self.ops.append(str(token.value))
            elif token.typ == TokenType.STR:
                self.ops.append(repr(token.value))
            elif token.typ == TokenType.CSTR:
                self.ops.append(str(token.value))
            elif token.typ == TokenType.CHAR:
                self.ops.append(str(token.value))
            elif token.typ == TokenType.KEYWORD:
                self.ops.append(token.text)
            else:
                assert False, "unreachable"

        
        # Init Break point
        self._current_break = len(self.ops) - 1
        self.ops[self._current_break] = "<Break> " + self.ops[self._current_break]

        # Print source code
        self.source_menu.add_item_list(self.ops)


    def select_break(self):
        """
        Update <Break> Flag
        """
        # Get selected item index
        self._selected_index = self.source_menu.get_selected_item_index()

        # Remove <Break> Flag
        self.ops[self._current_break] = self.ops[self._current_break][8:]

        # Add <Break> Flag
        self.ops[self._selected_index] = "<Break> " + self.ops[self._selected_index]
        
        # Updat break index
        self._current_break = self._selected_index

        # Rerender source code
        self.source_menu.clear()
        self.source_menu.add_item_list(self.ops)
        self.source_menu.set_selected_item_index(self._selected_index)


    def run_until_break(self):
        """
        Run program until breakpoint is reached
        """
        # Capture all stdout
        self._debugger_stdout = StringIO()
        self._debugger_stdout.buffer = BufferedRandom(raw=BytesIO())
        sys.stdout = self._debugger_stdout

        # Capture all stderr
        self._debugger_stderr = StringIO()
        self._debugger_stderr.buffer = BufferedRandom(raw=BytesIO())
        sys.stderr = self._debugger_stderr

        # Capture stdin
        #self._debugger_stdin = StringIO()
        #self._debugger_stdin.buffer = DebuggerStdin(self.root)
        #sys.stdin = self._debugger_stdin
        
        try:
            # Slice Program
            program: Program = Program(ops = self.program.ops[0:self._current_break], memory_capacity=self.program.memory_capacity)

            # Timing Start
            self._start_time = time.time()

            # Simulate program
            self.stack, self.mem = simulate_little_endian_linux(program, self.argv)
            
            # Render Stack
            self.stack.reverse()
            self.stack_menu.clear()
            self.stack_menu.add_item_list(self.stack)

            # Render Memory
            self._print_memory()

            # Print captured stdout
            self.update_console_output()
        except SystemExit as ex:
            if ex.code != 0:
                print(self._debugger_stderr.buffer)
                self._debugger_stderr.buffer.seek(0)
                self.root.show_error_popup("Completed with error", self._debugger_stderr.buffer.read().decode('utf-8'))

            self.update_console_output()
        
        except Exception as e:
            self.root.show_error_popup("Error on execution", e)

        # Restore default stdout
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
        sys.stdin = sys.__stdin__


    def run_next(self):
        """
        Run until next step
        """
        self._handle_step(1)

    def run_prev(self):
        """
        Run until previous step
        """
        self._handle_step(-1)

    def _handle_step(self, add: int):
        """
        Update <Beak> flag and run program until breakpoint
        """
        # Gatecondition
        if add > 0:
            if self._current_break == len(self.ops) - 1:
                return
        elif self._current_break == 0:
            return

        # Update <Break> flag
        self.ops[self._current_break] = self.ops[self._current_break][8:]
        self._current_break += add

        self.ops[self._current_break] = "<Break> " + self.ops[self._current_break]

        # Rerender source code
        self.source_menu.clear()
        self.source_menu.add_item_list(self.ops)

        # Tun til it breaks
        self.run_until_break()


    def _print_memory(self):
        """
        Render memory in human readable form
        """
        # Init helper variables
        mem_list = []
        i = 0

        # Skip pointer padding
        self.mem = self.mem[self._memory_start:]
        while len(self.mem) > 0:
            # Slice memory in 8 bit chunks
            chunk = self.mem[:8]
            
            # make chunk human readable
            byte_str = ' '.join(format(b, '02x') for b in chunk)
            human_readable = ''.join(str(x) for x in chunk.decode('utf-8') if str(x).isprintable())

            # Append to render list
            mem_list.append(f"{byte_str}    {human_readable}")

            # Update memory
            self.mem = self.mem[8:]
            
            # Gatecondition for performance
            i += 1
            if i >= self._memory_range:
                break

        # Rerender memory
        self.memory_menu.clear()
        self.memory_menu.add_item_list(mem_list)


    def update_console_output(self):
        """
        Render console output
        """

        # Add execution time to output
        self._debugger_stdout.buffer.write(b"\nExecution Time: %f seconds" % (time.time() - self._start_time))
        self._debugger_stdout.buffer.flush()

        # Startpoint for buffer
        self._debugger_stdout.buffer.seek(0)

        # Rerender output
        self.stdout.clear()
        self.stdout.add_item_list(self._debugger_stdout.buffer.read().decode('utf-8').splitlines())



def run_debugger(program_path: str, include_paths: List[str], expansion_limit: int, argv: List[str], memory_start=SIM_NULL_POINTER_PADDING, memory_range=MEM_VIEW_LIMIT):
    """
    Setup and start debugger
    """
    root = py_cui.PyCUI(10, 10)
    root.set_title('Porth Debugger v.{}'.format(__version__))

    # Create the wrapper instance object.
    frame = PorthDebugger(root, program_path, include_paths, expansion_limit, argv, memory_start, memory_range)
    
    # Start the CUI
    root.start()